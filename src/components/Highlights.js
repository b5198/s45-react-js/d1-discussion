import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(

		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>

						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum ratione voluptas libero autem, at adipisci itaque nihil magni omnis minus voluptatum laboriosam? Labore illo repellendus, ducimus libero quia suscipit at?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>

						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum ratione voluptas libero autem, at adipisci itaque nihil magni omnis minus voluptatum laboriosam? Labore illo repellendus, ducimus libero quia suscipit at?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of our community</h2>
						</Card.Title>

						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum ratione voluptas libero autem, at adipisci itaque nihil magni omnis minus voluptatum laboriosam? Labore illo repellendus, ducimus libero quia suscipit at?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		)
}