// import CourseCard from '../components/CourseCard';
// import coursesData from '../mockData/coursesData';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';

export default function CoursePage() {

	const [allCourses, setAllCourses] = useState([])
	
	const fetchData = () => {
		fetch('https://taracut.herokuapp.com/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllCourses(data)
		})
	}

	// it renders the function fetchData() => it gets the updated data coming from the fetch
	useEffect (() => {
		fetchData()
	}, [])
	// if the useEffect has no variables, it will only render one time

	const{ user } = useContext(UserContext);

	/*//Check if the mock data was captured
	console.log(coursesData);
	//PHP laravel
	console.log(coursesData[0]);

	//For us to be able to display all the courses from the data file we are going to use the map() method
	const courses = coursesData.map(individualCourse => {
		return(
			<CourseCard key={individualCourse.id} courseProp={individualCourse}/>
			//add key property to keep track the number of courses and to avoid duplication
			)
	})
*/
	return(
		<>
			<h1>Courses</h1>
			
			{(user.isAdmin === true) ?
				<AdminView coursesData = {allCourses} fetchData={fetchData} />

				:

				<UserView coursesData = {allCourses} />
			}
		</>


		)
}