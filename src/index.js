import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

const name = 'John Smith';
const element = <h1>Hello, {name}</h1>

// React Fragment (Blank <> or <React.Fragment>)
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
/*  <>
    <h1>Hello, {name}</h1>
    <h3>Full Stack Developer</h3>
    <h4>Welcome to my page</h4> */
  <App />
);

